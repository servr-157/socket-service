package com.kilo.service.websocket.presentation.controller;

import com.kilo.service.websocket.domain.chat.service.MessagingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
@Slf4j
public class MessagingController {

    private final MessagingService messagingService;
    @MessageMapping("/message")
    public void produceMessage(String message){
        log.info("Message receive {}", message);
        messagingService.produceMessage(message);
    }
}
