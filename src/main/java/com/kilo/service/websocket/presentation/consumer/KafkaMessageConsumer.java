package com.kilo.service.websocket.presentation.consumer;

import com.kilo.service.websocket.domain.chat.service.MessagingService;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KafkaMessageConsumer{
    private final MessagingService messagingService;
    @KafkaListener(topics = "chat-topic", groupId = "${spring.kafka.consumer.group-id}")
    public void consume(String message) {
        System.out.println(message);
        messagingService.produceMessage(message);
    }
}
