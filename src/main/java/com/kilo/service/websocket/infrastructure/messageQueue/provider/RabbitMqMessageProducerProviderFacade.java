package com.kilo.service.websocket.infrastructure.messageQueue.provider;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kilo.service.websocket.domain.chat.provider.MessageProducerProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Primary
public class RabbitMqMessageProducerProviderFacade implements MessageProducerProvider {

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final ObjectMapper mapper;
    @Override
    public void produce(String message) throws JsonProcessingException {
        JsonNode jsonNodeMessage = mapper.readTree(message);
        String conversationId = jsonNodeMessage.get("conversationId").asText();
        simpMessagingTemplate.convertAndSend("/topic/"+conversationId, message);
    }

}
