package com.kilo.service.websocket.infrastructure.config.websocket;


import com.kilo.service.websocket.infrastructure.config.rabbitmq.RabbitMqConfigProperty;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
@RequiredArgsConstructor
public class WebsocketConfig implements WebSocketMessageBrokerConfigurer {
    private final RabbitMqConfigProperty rabbitMqConfigProperty;
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws").setAllowedOriginPatterns("*");
    }



    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {


        registry
                .setApplicationDestinationPrefixes("/app")
                .enableStompBrokerRelay("/topic", "/queue")
                .setRelayHost(rabbitMqConfigProperty.getHost())
                .setRelayPort(rabbitMqConfigProperty.getPort())
                .setClientLogin(rabbitMqConfigProperty.getUsername())
                .setClientPasscode(rabbitMqConfigProperty.getPassword());
    }
}
