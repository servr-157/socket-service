package com.kilo.service.websocket.infrastructure.config.rabbitmq;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
@ConfigurationProperties(prefix = "spring.rabbitmq")
public class RabbitMqConfigProperty {
    private String host;
    private String username;
    private String password;
    private int port;
}
