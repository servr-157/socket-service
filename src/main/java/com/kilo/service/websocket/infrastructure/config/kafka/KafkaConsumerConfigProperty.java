package com.kilo.service.websocket.infrastructure.config.kafka;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "spring.kafka.consumer")
@Configuration
@Data
public class KafkaConsumerConfigProperty {
    private String bootstrapServers;
    private String groupId;
    private String autoOffsetReset;
}
