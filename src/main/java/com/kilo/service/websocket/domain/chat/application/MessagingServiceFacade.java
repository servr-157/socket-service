package com.kilo.service.websocket.domain.chat.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kilo.service.websocket.domain.chat.provider.MessageProducerProvider;
import com.kilo.service.websocket.domain.chat.service.MessagingService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Primary
public class MessagingServiceFacade implements MessagingService {
    private final MessageProducerProvider messageProducerProvider;
    @Override
    public void produceMessage(String message) {
        try {
            messageProducerProvider.produce(message);
        }catch (JsonProcessingException exception){
            throw new RuntimeException();
        }
    }
}
