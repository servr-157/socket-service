package com.kilo.service.websocket.domain.chat.provider;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface MessageProducerProvider {
    void produce(String message) throws JsonProcessingException;
}
