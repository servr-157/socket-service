package com.kilo.service.websocket.domain.chat.service;

public interface MessagingService {
    void produceMessage(String message);
}
